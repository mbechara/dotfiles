#!/bin/bash

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if [[ -d "$HOME/.local/bin" ]]; then
   PATH=$PATH:$HOME/.local/bin
fi

if [[ -d "$HOME/bin" ]]; then
   PATH=$PATH:$HOME/bin
fi

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
	for rc in ~/.bashrc.d/*; do
		if [ -f "$rc" ]; then
			. "$rc"
		fi
	done
fi

colorscript --random
