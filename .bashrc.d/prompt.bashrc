#!/bin/bash

###############################################################
# Private function that prints colored exit code
###############################################################
__prompt_exit_code()
{
   exit_code=$?
   if [[ $exit_code == 0 ]]; then
      echo -e "\e[0;32m$exit_code\e[0m" 
   else
      echo -e "\e[0;31m$exit_code\e[0m"
   fi
}

###############################################################
# Private function that prints colored git branch if found
###############################################################
__prompt_git()
{
   if git branch > /dev/null 2>&1; then
      echo -e " (\e[0;32mgit:$(git branch 2>&1 | grep '\*' | cut -b 3-)\e[0m)"
   fi
}

###############################################################
# Bash prompt
###############################################################
PS1='\n\
\[\e[1;30m\]\342\224\214\342\224\200\[\e[0m\]\
[$(__prompt_exit_code)]\
\342\224\200\
[\[\e[1;34m\]\u\[\e[0m\]@\[\e[1;33m\]\h\[\e[0m\]]\
\342\224\200\
[\[\e[1;36m\]\W\[\e[0m\]]\
$(__prompt_git)\
\n\
\[\e[1;30m\]\342\224\224\342\224\200\342\224\200\076\[\e[0m\] '
