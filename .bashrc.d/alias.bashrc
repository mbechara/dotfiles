#!/bin/bash

###############################################################
# Replacing ls with eza
###############################################################
alias ls='eza -al --color=always --group-directories-first'
alias la='eza -a --color=always --group-directories-first'
alias ll='eza -l --color=always --group-directories-first'
alias l.='eza -a | grep "^\."'

###############################################################
# Use to locate problem with vim
###############################################################
alias rawvim='vim -u NONE -U NONE -N -i NONE'
alias noviminfo='vim -u NONE -U NONE -N'
alias novimplugin='vim -u ~/.vimrc --noplugin -N -i NONE'

###############################################################
# Adding options
###############################################################
alias grep='grep --color=auto'
alias df='df -h'

###############################################################
# Shorter version of the command
###############################################################
alias cls='clear'
