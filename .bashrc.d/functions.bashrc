#!/bin/bash

###############################################################
# Function that goes n directories up
###############################################################
up()
{
   local directories_up=${1:-1}
   for (( i=0 ; i < $directories_up ; i++ )); do
      cd ..
   done
}

###############################################################
# Function that extracts archive
###############################################################
extract()
{
   if [[ -z $1 ]]; then
      echo -e "\e[31merror\e[0m: no input file"
      return 1
   fi
   if [[ ! -f $1 ]]; then
      echo -e "\e[31merror\e[0m: $1 does not exist"
      return 1
   fi
   case $1 in
      *.tar.bz2) tar xvjf $1 ;;
      *.tar.gz)  tar xvzf $1 ;;
      *.tar.xz)  tar xvJf $1 ;;
      *.tbz2)    tar xvjf $1 ;;
      *.tgz)     tar xvzf $1 ;;
      *.bz2)     bunzip2 $1 ;;
      *.rar)     rar x $1 ;;
      *.gz)      gunzip $1 ;;
      *.zip)     unzip $1 ;;
      *.Z)       uncompress $1 ;;
      *.xz)      xz -d $1 ;;
      *.7z)      7z x $1 ;;
      *.a)       ar x $1 ;;
      *)         echo -e "\e[31merror\e[0m: Unable to extract $1";;
   esac
}
