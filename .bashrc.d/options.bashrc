#!/bin/bash

###############################################################
# Shell options
###############################################################
shopt -q -o emacs
shopt -s extglob
shopt -s autocd
shopt -s cdspell
shopt -s cmdhist

###############################################################
# Readline variables
###############################################################
bind 'set completion-ignore-case on'
