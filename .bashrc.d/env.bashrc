#!/bin/bash

###############################################################
# Export
###############################################################
export EDITOR=vim
export VISUAL=vim
export SYSTEMD_EDITOR=vim
export TERM="xterm-256color"
export HISTCONTROL=ignoredups:erasedups
